# Gated Dropbox Asset Manager

This tool leverages the Dropbox API to create a user permission based asset manager. This system was impelemented so that a client could log in, see organized groups of assets, and quickly preview/download the asset that they needed.

![demo gif](https://gitlab.com/teustice/gated-dropbox-asset-manager/raw/master/demo.gif)

