import React from 'react';
import renderer from 'react-test-renderer';
import LightBox from './components/LightBox';
import ToggleSwitch from './components/ToggleSwitch';
import Sidebar from './components/Sidebar';

test('LightBox renders without crashing', () => {
  const component = renderer.create(
    <LightBox />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('ToggleSwitch renders without crashing', () => {
  const component = renderer.create(
    <ToggleSwitch />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Sidebar renders without crashing', () => {
  const component = renderer.create(
    <Sidebar />,
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
