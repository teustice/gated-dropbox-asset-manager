import React, { Component } from 'react';
import OktaSignIn from '@okta/okta-signin-widget';
import logo from '../assets/images/logo.png';
import {getAuthBaseUrl} from '../lib/Helpers';

export default class LoginPage extends Component{
  constructor(){
    super();
    this.state = {
      user:null,
      widget: new OktaSignIn({
        logo: logo,
        baseUrl: getAuthBaseUrl(),
        clientId: '0oaic8ldstnMpZ5xo0h7',
        redirectUri: 'https://cbdam.tannereustice.com/',
        authParams: {
          issuer: 'default',
          responseType: 'id_token'
        },
        helpLinks: {
          help: 'mailto:dam@cheshirebeane.com',
        },
      })
    };
    this.showLogin = this.showLogin.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount(){
    this.state.widget.session.get((response) => {
      if(response.status !== 'INACTIVE'){
        this.props.setUser(response.login);
      }else{
        this.showLogin();
      }
    });
    if (this.props.user) {
      this.sendLogoutText(
        <div>
          <p>Welcome, {this.props.user}</p>
          <button className="btn_1" onClick={this.logout}>Logout</button>
        </div>
      )
    }
  }

  componentDidUpdate() {
      if (this.props.user) {
        this.sendLogoutText(
          <div>
            <p>Welcome, {this.props.user}</p>
            <button className="btn_1" onClick={this.logout}>Logout</button>
          </div>
        )
      }
  }

  showLogin(){
    this.state.widget.renderEl({el:this.loginContainer},
      (response) => {
        if(response.claims) {
          this.props.setUser(response.claims.email);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  logout(){
    this.state.widget.signOut(() => {
      this.props.setUser(null);
      this.showLogin();
    });
  }

  sendLogoutText(html) {
    this.props.setLogout(html);
  }

  render(){
    return(
      <div className="logout">

        {this.props.user ? null : (
          <div ref={(div) => {this.loginContainer = div; }} />
        )}
      </div>
    );
  }
}
