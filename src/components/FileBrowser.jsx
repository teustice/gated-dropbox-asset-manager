import React, { Component, Fragment } from 'react';
import fetch from 'isomorphic-fetch';
import {Dropbox} from 'dropbox';

import LoadingSpinner from '../components/LoadingSpinner';
import LightBox from '../components/LightBox';
import Sidebar from '../components/Sidebar';
import ToggleSwitch from '../components/ToggleSwitch';

import folderIcon from '../assets/images/folder.svg';
import backButton from '../assets/images/back-button.svg';

const NUMBERTOFETCH = 24;

class FileBrowser extends Component {

  constructor() {
    super();

    this.state = {
      dbx: undefined,
      rootFolders: [],
      folders: [],
      files: [],
      imgSources: [],
      rootDir: '',
      currentDir: undefined,
      prevDir: [],
      loading: false,
      currentPage: null,
      currentFiles: [],
      totalPages: null,
      totalFileCount: null,
      currentPreview: null,
      lightBoxOpen: false,
      dbxCursor: null,
      dbxPageCache: [],
      dbxCachePos: 0,
      hasMore: null,
      isGallery: false,
    }
  }

  componentDidMount() {
    let dbx = new Dropbox({ accessToken: 'YOUR_TOKEN_HERE', fetch: fetch });

    this.setState({
      dbx: dbx
    });

  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.files !== prevState.files) {
      //load thumbnails
      let that = this;
      let images = document.querySelectorAll('.files .image img');
      let imgPaths = [];
      images.forEach(function(img, index){
        imgPaths.push(img.dataset.path);
      })

      //get images non-batch
      images.forEach(function(img, index){
        if(!img.src) {
          that.getImageThumbnail(img.dataset.path, img)
        }
      })

    }
  }

  isImg(filename) {
    return (/\.(gif|jpg|jpeg|tiff|png)$/i).test(filename);
  }

  isPreviewable(filename) {
    return (/\.(ai|doc|docm|docx|eps|odp|odt|pps|ppsm|ppsx|ppt|pptm|pptx|rtf|csv|ods|xls|xlsm|xlsx)$/i).test(filename);
  }

  onPageChanged = data => {
    let allFiles = this.state.folders.concat(this.state.files);
    let { currentPage, totalPages, pageLimit } = data;

    let offset = (currentPage - 1) * pageLimit;

    let currentFiles = allFiles.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentFiles, totalPages });
  }

  getImageThumbnail(path, imgEl){
    let that = this;
    if(!imgEl.src) {
      let size = this.state.isGallery ? 'w480h320' : 'w480h320';

      this.state.dbx.filesGetThumbnail({"path": path, "size": size})
      .then(function(response) {
        if(imgEl) {
          imgEl.src = window.URL.createObjectURL(response.fileBlob);
          let imgSources = that.state.imgSources.concat(
            {url: window.URL.createObjectURL(response.fileBlob), path: imgEl.dataset.path}
          )
          that.setState({imgSources: imgSources});
        }
      })
      .catch(function(error) {
        console.log("got error:");
        console.log(error);
      });

    }
  }

  getImageThumbnailBatch(paths){
      let imgEntries = [];
      let size = this.state.isGallery ? 'w480h320' : 'w64h64';
      paths.forEach(function(path, index) {
        imgEntries.push({path: path, size: size});
      })
      this.state.dbx.filesGetThumbnailBatch({entries: imgEntries})
      .then(function(response) {
        if(response.entries) {
          let images = document.querySelectorAll('.files .image img');
          images.forEach(function(img, index){
            response.entries.forEach(function(entry, index){
              if(img.dataset.path === entry.metadata.path_lower) {
                img.src = 'data:image/jpg;base64,' + entry.thumbnail;
              }
            })
          })

        }
      })
      .catch(function(error) {
        console.log("got error:");
        console.log(error);
      });
  }

  isGallery(folderName) {
    let nameArray = folderName.split(' ');
    let lastWord = nameArray[nameArray.length - 1];

    if(lastWord === '_gallery') {
      this.setState({isGallery: true})
    } else {
      this.setState({isGallery: false})
    }
  }

  renderFiles(folder) {
    //recursively loop through folders
    let that = this;

    this.setState({loading: true});
    this.setState({currentDir: folder});

    // this.isGallery(folder); //set Gallery state

    //get folders seperately
    this.state.dbx.filesListFolder({path:folder})
    .then(function(response){
      // that.renderFiles(file.path_lower);
      let newFolders = [];
      response.entries.forEach(function(file, index){
        if (file['.tag'] === 'folder') {
          if(!that.state.files.includes(<p className="folder" key={file.name} onClick={() => that.getFileDownload(file.path_lower)}>{file.name}</p>)) {

            if(that.props.groups) {
              //only show folders for this group
              let folderPermission = false;
              that.props.groups.forEach(function(group, index) {
                if((folder === that.state.rootDir && file.name === group.profile.name) || folder !== that.state.rootDir || group.profile.name === 'Admin') {
                  folderPermission = true;
                }
              })

              if(folderPermission) {
                newFolders = newFolders.concat(
                  <div className="folder file" key={file.name} onClick={() => that.getFolder(file.path_lower)}>
                    <img src={folderIcon} alt=""/>
                    <div className="info">

                      <p key={file.name} >{file.name}</p>
                    </div>
                  </div>
                )
                if(folder === that.state.rootDir) {
                  let rootFolder = that.state.rootFolders.concat(
                    <li key={file.name + '1'} onClick={() => that.getFolder(file.path_lower, true)} ><a>{file.name}</a></li>
                  )
                  that.setState({rootFolders: rootFolder});
                }
              }
            }
          }
        }
      })
      that.setState({folders: newFolders});
    })
    .catch(function(error) {
      console.error(error);
    });

      this.state.dbx.filesListFolder({path: folder, limit: NUMBERTOFETCH})
      .then(function(response) {
        that.setState({
          dbxCursor: response.cursor,
          dbxPageCache: [],
          dbxCachePos: 0,
          hasMore: response.has_more,
          totalFileCount: response.entries.length
        });

        let newFiles = [];
        response.entries.forEach(function(file, index){

          if (file['.tag'] === 'folder') {

          } else {
              if(that.isImg(file.name)) {
                    newFiles = newFiles.concat(
                      <div className="file image" key={file.name}>
                        <div className={`img-box`}>
                          <div className={`${that.state.isGallery ? 'img-load-bg' : ''}`}></div>
                          <img id={`image-${index}`} data-path={file.path_lower} alt="" onClick={() => that.renderLightBox(file.path_lower)} />
                        </div>
                        <div className="info">
                          <a href={response.link} onClick={() => that.renderLightBox(file.path_lower)}><p>{file.name}</p></a>
                          <ul className="actions">
                            <li>
                              <a onClick={() => that.getFileDownload(file.path_lower)}>DOWNLOAD</a>
                            </li>
                            {
                              that.isImg(file.path_lower) &&
                              <li>
                                |
                              </li>
                            }
                            {
                              that.isImg(file.path_lower) &&
                              <li>
                                <a onClick={() => that.renderLightBox(file.path_lower)}>PREVIEW</a>
                              </li>
                            }
                          </ul>
                        </div>
                      </div>
                  )
              } else {
                newFiles = newFiles.concat(
                  <div className="file" key={file.name}>
                    <p key={file.name}>{file.name}</p>
                    <ul className="actions">
                      <li>
                        <a onClick={() => that.getFileDownload(file.path_lower)}>DOWNLOAD</a>
                      </li>
                      {
                        that.isImg(file.path_lower) &&
                        <li>
                          <li>|</li>
                        </li>
                      }
                      {
                        that.isImg(file.path_lower) &&
                        <li>
                          <a onClick={() => that.renderLightBox(file.path_lower)}>PREVIEW</a>
                        </li>
                      }
                    </ul>
                  </div>
                )
              }
          }

        });
        that.setState({files: newFiles});
        let pageCache = that.state.dbxPageCache.concat([newFiles]);
        that.setState({dbxPageCache: pageCache});
        that.setState({loading: false});


      })
      .catch(function(error) {
        console.error(error);
      });

  }

  loadMore(cursor, isPrev) {
    let that = this;


    let cachePos = this.state.dbxCachePos + 1;

    this.removeFiles();

    //LOAD FROM CACHE IF EXISTS
    if(cachePos < this.state.dbxPageCache.length) {
      that.setState({files: this.state.dbxPageCache[cachePos]});
      that.setState({dbxCachePos: cachePos});
    } else {
      //append to page cache with new page

      that.setState({dbxCachePos: cachePos});

      this.setState({loading: true});

      this.state.dbx.filesListFolderContinue({cursor: cursor})
      .then(function(response) {


        that.setState({dbxCursor: response.cursor}); //set cursor for pagination
        that.setState({hasMore: response.has_more});
        that.setState({totalFileCount: response.entries.length});

        let newFiles = [];
        response.entries.forEach(function(file, index){

          if (file['.tag'] === 'folder') {

          } else {
            if(that.isImg(file.name)) {
              newFiles = newFiles.concat(
                <div className="file image" key={file.name}>
                  <div className={`img-box`}>
                    <div className={`${that.state.isGallery ? 'img-load-bg' : ''}`}></div>
                    <img id={`image-${index}`} data-path={file.path_lower} alt="" onClick={() => that.renderLightBox(file.path_lower)} />
                  </div>
                  <div className="info">
                    <a href={response.link} onClick={() => that.renderLightBox(file.path_lower)}><p>{file.name}</p></a>
                    <ul className="actions">
                      <li>
                        <a onClick={() => that.getFileDownload(file.path_lower)}>DOWNLOAD</a>
                      </li>
                      {
                        that.isImg(file.path_lower) &&
                        <li>
                          |
                        </li>
                      }
                      {
                        that.isImg(file.path_lower) &&
                        <li>
                          <a onClick={() => that.renderLightBox(file.path_lower)}>PREVIEW</a>
                        </li>
                      }
                    </ul>
                  </div>
                </div>
              )
            } else {
              newFiles = newFiles.concat(
                <div className="file" key={file.name}>
                  <p key={file.name}>{file.name}</p>
                  <ul className="actions">
                    <li>
                      <a onClick={() => that.getFileDownload(file.path_lower)}>DOWNLOAD</a>
                    </li>
                    {
                      that.isImg(file.path_lower) &&
                      <li>
                        <li>|</li>
                      </li>
                    }
                    {
                      that.isImg(file.path_lower) &&
                      <li>
                        <a onClick={() => that.renderLightBox(file.path_lower)}>PREVIEW</a>
                      </li>
                    }
                  </ul>
                </div>
              )
            }
          }

        });
        that.setState({files: newFiles});
        that.setState({loading: false});

        //update page cache
        let pageCache = that.state.dbxPageCache.concat([newFiles]);
        that.setState({dbxPageCache: pageCache});


      })

    }

  }

  getFileDownload(path) {
    this.state.dbx.filesGetTemporaryLink({path: path})
      .then(function(res) {
        if(res.link) {
          window.location.href = res.link;
        }
      })
  }

  renderLightBox(path) {
    let that = this;
    this.state.dbx.filesGetTemporaryLink({path: path})
      .then(function(res) {
        that.setState({currentPreview: res.link})
        that.setState({lightBoxOpen: true});
      })
  }

  removeFiles() {
    this.setState({files: []});
    // this.setState({folders: []});
  }

  getFiles() {
    if(!this.state.loading) {
      this.removeFiles();
      this.renderFiles(this.state.rootDir);
    }
  }

  getFolder(path, isTopLevel) {
    if(!this.state.loading) {
      this.removeFiles();
      this.removeFiles();

      //set directory state
      if(isTopLevel) {
        this.setState({prevDir: ['']});
      } else {
        let breadcrumbs = this.state.prevDir.concat(this.state.currentDir);
        this.setState({prevDir: breadcrumbs});
      }
      this.setState({currentDir: path});

      this.renderFiles(path);
    }

  }

  getPrevFolder() {
    if(!this.state.loading) {

      this.removeFiles();

      let dir = this.state.prevDir.slice(-1)[0]; //get previous page
      //set directory state
      let breadcrumbs = this.state.prevDir;
      breadcrumbs.pop();

      this.setState({prevDir: breadcrumbs});
      this.setState({currentDir: dir});

      this.renderFiles(dir);
    }
  }

  isLoading() {
    if(this.state.totalFileCount !== (this.state.files.length + this.state.folders.length) && !(this.state.files.length + this.state.folders.length >= NUMBERTOFETCH)) {
      if(this.state.loading !== true) {
        this.setState({loading: true});
      }
    } else {
      if(this.state.loading === true) {
        this.setState({loading: false});
      }
    }
  }

  closeModal() {
    this.setState({lightBoxOpen: false});
  }

  loadPrevFromCache() {
    let cachePos = this.state.dbxCachePos - 1;

    if (cachePos <= 0) {
      this.setState({files: this.state.dbxPageCache[0]});
      this.setState({dbxCachePos: 0});
    } else {
      this.setState({files: this.state.dbxPageCache[cachePos]});
      this.setState({dbxCachePos: cachePos});
    }
  }

  loadGalleryView() {
    this.setState({isGallery: true});
  }

  loadListView() {
    this.setState({isGallery: false});
  }

  render() {

    if(this.state.loading && document) {
      let element = document.getElementById('spinner');
      element.style.opacity = 1;
    } else {
      if(document) {
        let element = document.getElementById('spinner');
        if(element) {
          element.style.opacity = 0;
        }
      }
    }
    return (

      <Fragment>

        <Sidebar rootFolders={this.state.rootFolders} logoutHtml={this.props.logoutHtml} />

        <div className="file-browser-wrapper main-area">
          <div className={`file-browser ${this.state.loading ? 'is-loading' : ''}`} >
            <ToggleSwitch value='Gallery View' click={this.state.isGallery ? () => this.loadListView() : () => this.loadGalleryView()}/>

            <div className="pagination">
              {this.state.dbxCachePos !== 0 &&
                <button className="btn_1" onClick={() => this.loadPrevFromCache()} >Prev Page</button>
              }
              {(this.state.hasMore || (this.state.dbxCachePos < this.state.dbxPageCache.length - 1 && this.state.dbxPageCache.length > 1)) && this.state.dbxCursor &&
                <button className="btn_1" onClick={() => this.loadMore(this.state.dbxCursor)} >Next Page</button>
              }
            </div>

            <div className="right">
              <div className={`files ${this.state.isGallery ? 'gallery' : '' }`}>
                {this.state.prevDir.length > 1 &&
                  <p onClick={() => this.getPrevFolder()} className="back-button"><a><img src={backButton} alt=""/></a></p>
                }
                {this.state.folders}
                {this.state.files}
              </div>
            </div>
          </div>


          <LoadingSpinner loading={this.state.loading}></LoadingSpinner>
          <LightBox
            ref={this.LightBoxRef}
            isOpen={this.state.lightBoxOpen}
            closeModal={this.closeModal.bind(this)}
            src={this.state.currentPreview}>
          </LightBox>
        </div>
      </Fragment>

    );
  }

}

export default FileBrowser;
