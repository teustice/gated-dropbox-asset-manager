import React, { Component } from 'react';

class LightBox extends Component {

  constructor() {
    super();

    this.state = {
    }
  }

  closeModal() {
    this.props.closeModal();
  }

  render() {
    return (
      <div className="lightbox" style={{display: (this.props.isOpen ? 'flex' : 'none')}}>
        <a className="close" onClick={() => this.closeModal()}>X</a>
        <div className="close-bg" onClick={() => this.closeModal()}></div>
        <div className="content" style={{opacity: (this.props.isOpen ? '1' : '0'), transition: 'all 400ms'}}>
          {this.props.isOpen &&
            <img src={this.props.src} alt=""></img>
          }
        </div>
      </div>
    );
  }

}

export default LightBox;
