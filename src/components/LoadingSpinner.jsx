import React, { Component } from 'react';
import {Spinner} from 'spin.js';

// tanner personal QI_6NCpCv-AAAAAAAAAAMVk0g2Ox16xQprkl2rTt3Fwtux_Ql_nuOrmUgAqtw-1-
// cb gzL793fQAJAAAAAAAAAsaRd74JCbDyGZwcQChF21DBheq5SIQGGes67qK8MVl91Y

class LoadingSpinner extends Component {

  constructor() {
    super();

    this.state = {
      spinner: undefined,
    }
  }

  componentDidMount() {
    //init Spinner
    var opts = {
      lines: 13, // The number of lines to draw
      length: 1, // The length of each line
      width: 17, // The line thickness
      radius: 45, // The radius of the inner circle
      scale: .3, // Scales overall size of the spinner
      corners: 0, // Corner roundness (0..1)
      color: '#000', // CSS color or array of colors
      fadeColor: 'transparent', // CSS color or array of colors
      speed: 1, // Rounds per second
      rotate: 0, // The rotation offset
      animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
      direction: 1, // 1: clockwise, -1: counterclockwise
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      className: 'spinner', // The CSS class to assign to the spinner
      top: '100px', // Top position relative to parent
      shadow: '0 0 1px transparent', // Box-shadow for the lines
      position: 'fixed' // Element positioning
    };

    var target = document.getElementById('spinner');
    target.style.opacity = 0;

    this.setState({spinner: new Spinner(opts).spin(target)});
  }

  render() {
    if(this.props.loading && document) {
      let element = document.getElementById('spinner');
      element.style.opacity = 1;
    } else {
      if(document) {
        let element = document.getElementById('spinner');
        if(element) {
          element.style.opacity = 0;
        }
      }
    }
    return (
      <div id="spinner" className="spinner"></div>
    );
  }

}

export default LoadingSpinner;
