import React, { Component } from 'react';

class Pagination extends Component {

  render() {
    return (
      <div className="pagination">
        {this.props.dbxCachePos !== 0 &&
          <button className="btn_1" onClick={() => this.loadPrevFromCache()} >Prev Page</button>
        }
        {(this.props.hasMore || (this.props.dbxCachePos < this.props.dbxPageCache.length - 1 && this.props.dbxPageCache.length > 1)) && this.props.dbxCursor &&
          <button className="btn_1" onClick={() => this.loadMore(this.props.dbxCursor)} >Next Page</button>
        }
      </div>
    );
  }

}

export default Pagination;
