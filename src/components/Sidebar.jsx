import React, { Component } from 'react';
import logo from '../assets/images/logo.png';

class Sidebar extends Component {

  render() {
    return (
      <header className="sidebar-wrapper">
        <nav className="sidebar-nav main-nav">
          <img src={logo} alt=""/>
          <ul>
            {this.props.rootFolders}
          </ul>
        </nav>

        {this.props.logoutHtml &&
          <div className="logout">
            {this.props.logoutHtml}
          </div>
        }
      </header>
    );
  }

}

export default Sidebar;
