import React, { Component } from 'react';

class ToggleSwitch extends Component {

  render() {
    return (
      <div className="switch-box">
        <p>{this.props.value}</p>
        <label className="switch">
          <input type="checkbox" onClick={this.props.click} />
          <span className="slider"></span>
        </label>
      </div>
    );
  }

}

export default ToggleSwitch;
