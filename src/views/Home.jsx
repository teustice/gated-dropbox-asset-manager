import React, { Component } from 'react';

import FileBrowser from '../components/FileBrowser';
import LoginPage from '../components/Auth';
import {
  getAuthBaseUrl,
  getAuthToken
} from '../lib/Helpers';



class Home extends Component {

  constructor() {
    super();

    this.state = {
      user:null,
      currentUser: null,
      groups: null,
      logoutHTML: null,
    }

    this.fileBrowserRef = React.createRef()
    this.loginRef = React.createRef()
  }

  setUser(user) {
    this.setState({user: user});

    if(this.state.user) {
      this.setCurrentUser();
    }
  }

  setLogoutHTML(html) {
    if(!this.state.logoutHTML){
      this.setState({
        logoutHTML: html
      })
    }
  }

  setCurrentUser() {
    let that = this;
    fetch(`${getAuthBaseUrl()}/api/v1/users/${this.state.user}`, {
      mode: 'cors',
      credentials: "same-origin",
      cache: "no-cache",
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": `SSWS ${getAuthToken()}`
      }

    }).then(function(response) {
        return response.json();
    }).then(function(response) {
        that.setState({currentUser: response});

        that.getUserGroups();
    })
  }

  getUserGroups() {
    let that = this;
    if(this.state.currentUser) {
      fetch(`${getAuthBaseUrl()}/api/v1/users/${this.state.currentUser.id}/groups`, {
        mode: 'cors',
        credentials: "same-origin",
        cache: "no-cache",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": `SSWS ${getAuthToken()}`
        }

      }).then(function(response) {
        return response.json();
      }).then(function(response) {
        that.setState({groups: response});
        that.fileBrowserRef.current.getFiles();
      })
    }
  }



  render() {
    return (
      <div className="Main">
        <div>
          <LoginPage ref={this.loginRef} user={this.state.user} setUser={this.setUser.bind(this)} setLogout={this.setLogoutHTML.bind(this)}/>

          {this.state.user && this.state.groups &&
            <FileBrowser ref={this.fileBrowserRef} groups={this.state.groups} logoutHtml={this.state.logoutHTML}></FileBrowser>
          }
        </div>
      </div>
    );
  }

}

export default Home;
